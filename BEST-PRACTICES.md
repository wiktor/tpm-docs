---
title: Best practices
date: 2022-02-03
---

# Best practices

TPM-backed keys have specifics that are unique to TPMs but several of these guidelines are more generic and apply to all hardware tokens in general.

## Do: Generate signing keys in TPM

Key generated purely in TPM can take advantage of advanced TPM features like locking the keys to PCR registers.
Since OpenPGP allows having multiple signing keys attached to one primary key there is no downside of using one signing-key per device.

Additionally when the target device is broken, stolen or de-provisioned the signing subkey can be selectively revoked.

## Do: Migrate encryption keys to TPM

Generating encryption keys in the TPM can be dangerous: when the device is broken all communication and encrypted files effectively become unusable.

One way to resolve this conflict would be to generate one encryption key per device but that unfortunately requires all participating parties to cooperate and encrypt plaintext to all valid encryption subkeys.
Sequoia PGP by default does that but other popular OpenPGP software (e.g. GnuPG) does not.

So: migrate the encryption key using secure key duplication (as described in the [END-USER](END-USER) guide).

## Do: Set strong authentication value

TPMs use lockout counter to avoid brute-force attacks against keys but it is still reasonable to use strong password or passphrase to protect keys.

## Don't: Use unwrapped private keys

While the library is flexible enough to work with raw private keys using them defeats the entire premise of using a TPM.

Instead do: securely migrate the private key using parent key and the `wrap` utility.

## Do: Use strong cryptographic algorithms

Several TPM vendors already constrain the number and parameter of algorithms that can be used on their platform.
Even if the algorithms are available the minimal requirements for algorithms in 2022 are: RSA 2048 bits and NIST-P256.

## Do: Use separate keys for signing and encryption

OpenPGP allows using one key for signing *and* encryption but this is a bad idea.
The general consensus in the cryptographic community is that [one key should be used for only one purpose][RSA].
Use two different keys, one for signing, one for encryption.

[RSA]: https://crypto.stackexchange.com/a/12138

## Don't: clear the TPM unnecessarily

Clearing the TPM resets the internal seed values used to persist keys and will result in all stored keys being unusable.
This can be used to securely de-provision the device before it being thrown away as it makes sure anyone that will get hold of the device cannot use keys stored in it.
If done carelessly this will make private keys completely unusable and for some keys this can be disastrous (see also *Do: Migrate encryption keys to TPM* for a way to mitigate this problem).

Instead do: clear the TPM only when de-provisioning the device.
