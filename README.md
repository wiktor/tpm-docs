---
title: TPM documentation
date: 2022-02-03
---

# TPM documentation

This project lists resources associated with TPM support in Sequoia PGP.

For a quick introduction on how to use it see the [end-user guide](END-USER) or the [best practices](BEST-PRACTICES) guide.

## Project documentation

As part of the project several smaller projects have been developed:

  - [`pks`][] - [Private Key Store][] Protocol specification for accessing and using cryptographic keys stored on hardware tokens,
  - [`pks-tpm`][] - a [Private Key Store][] server which exposes TPM keys through a protocol understood by `sq` and the associated tools,
  - [`sq-pks`][] - set of lower-level extensions to `sq` for adding subkeys,
  - [`declarative-tpm`][] - tools for managing TPM keys via simple descriptive files ([documentation][tpm-openpgp-doc]),
  - [`sq` has been extended] to work with Private Key Stores via the `--private-key-store` parameter,
  - [`rust-tss-esapi` crate has been improved] to add API for key duplication as well as functions needed for ECDH derivation.

[`pks-tpm`]: https://gitlab.com/sequoia-pgp/pks-tpm
[`pks`]: https://gitlab.com/sequoia-pgp/pks
[Private Key Store]: https://gitlab.com/sequoia-pgp/pks
[`sq-pks`]: https://gitlab.com/wiktor/sq-pks
[`declarative-tpm`]: https://gitlab.com/wiktor/tpm-openpgp
[tpm-openpgp-doc]: https://wiktor.gitlab.io/tpm-openpgp/
[`sq` has been extended]: https://gitlab.com/sequoia-pgp/sequoia/-/merge_requests/1149
[`rust-tss-esapi` crate has been improved]: https://github.com/parallaxsecond/rust-tss-esapi/commits?author=wiktor-k

In the bigger PKS ecosystem there are also:

  - [`pks-openpgp-card`][] - a [Private Key Store][] server which allows using OpenPGP Cards in Sequoia PGP,
  - [`pkcs11-openpgp`][] - for smartcards that use PKCS#11 (highly *experimental*),
  - [`ssh-agent-pks`][] - SSH Agent integration that uses Private Key Store protocol.

[`pks-openpgp-card`]: https://gitlab.com/sequoia-pgp/pks-openpgp-card
[`pkcs11-openpgp`]: https://gitlab.com/wiktor/pkcs11-openpgp/
[`ssh-agent-pks`]: https://gitlab.com/sequoia-pgp/ssh-agent-pks

## Presentations

High level view of the entire TPM and PKS architecture is available in the following presentations:

  1. Adding TPM Support to Sequoia PGP - [slides](https://github.com/wiktor-k/tpm-presentation), [recording](https://nlnet.nl/events/20211123/PGP/index.html),
  1. Using private keys in Sequoia (PKS) - [slides](https://github.com/wiktor-k/pks-presentation/).

## Funding

Work on this project has been sponsored by the [NLnet Foundation][]. See [Adding TPM Support to Sequoia PGP][] project page for details.

[NLnet Foundation]: https://nlnet.nl/
[Adding TPM Support to Sequoia PGP]: https://nlnet.nl/project/Sequoia-TPM/
