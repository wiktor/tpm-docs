FROM ubuntu as build

RUN apt-get update && apt-get install -y pandoc

COPY *.md template.html /public/
WORKDIR /public

RUN pandoc --standalone --template template.html END-USER.md -o END-USER.html
RUN pandoc --standalone --template template.html README.md -o index.html
RUN pandoc --standalone --template template.html BEST-PRACTICES.md -o BEST-PRACTICES.html

# See: https://github.com/moby/moby/issues/16079#issuecomment-801240270
FROM scratch AS artifacts
COPY --from=build /public /
